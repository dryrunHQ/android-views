package com.osiris.priya.androidviews;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
//        Intent intent = new Intent(this,Test.class);
//        startActivity(intent);
    }

    /*
    * This method is called when user selects Menu button
    */
    @Override
    public boolean onCreateOptionsMenu(Menu menu){
        super.onCreateOptionsMenu(menu);
        menu.add(0, 0, 0, "AutoComplete");
        menu.add(0, 1, 0, "Button");
        menu.add(0, 2, 0, "CheckBox");
        menu.add(0, 3, 0, "EditText");
        menu.add(0, 4, 0, "RadioGroup");
        menu.add(0, 5, 0, "Spinner");
        return  true;
    }
    /*
    *  This method is invoked whenever user selects any item in the Menu
    */
    @Override
    public boolean onOptionsItemSelected(MenuItem item){
        switch (item.getItemId()){
            case 0:
                showAutoComplete();
                return  true;
            case 1:
                return  true;
            case 2:
                return  true;
            case 3:
                return  true;
            case 4:
                return  true;
            case 5:
                showSpinner();
                return  true;

        }
        return  true;
    }

    public void showAutoComplete(){
        Intent autoComplete = new Intent(this,AutoComplete.class);
        startActivity(autoComplete);
    }

    public void showSpinner(){
        Intent testSpinner = new Intent(this, testSpinner.class);
        startActivity(testSpinner);
    }
}
