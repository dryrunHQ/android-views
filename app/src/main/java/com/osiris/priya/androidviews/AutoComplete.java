package com.osiris.priya.androidviews;

import android.app.Activity;
import android.graphics.Color;
import android.os.Bundle;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Button;

/**
 * Created by priya on 6/15/2016.
 */


public class AutoComplete extends Activity {

    static final String[] months = new String[]{
            "January", "February", "March", "April", "May", "June", "July", "August", "September",
            "October", "November", "December"
    };

    @Override
    public void onCreate(Bundle savedInstance) {
        super.onCreate(savedInstance);
        setContentView(R.layout.autocomplete);

        ArrayAdapter<String> arrayAdapter = new ArrayAdapter<String>(this, android.R.layout.simple_list_item_1, months);
        final AutoCompleteTextView autoCompleteTextView = (AutoCompleteTextView) findViewById(R.id.testAutoComplete);
        autoCompleteTextView.setAdapter(arrayAdapter);

        final Button firstButton = (Button) findViewById(R.id.autoCompleteButton);
        firstButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                changeOption(autoCompleteTextView);
            }
        });

        final Button secondButton = (Button) findViewById(R.id.textColorButton);
        secondButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                changeTextColor(autoCompleteTextView);
            }
        });
    }
        public void changeOption(AutoCompleteTextView atv){
        if(atv.getHeight()==100){
            atv.setHeight(30);
        }
        atv.setHeight(100);
    }
        public void changeTextColor(AutoCompleteTextView atv){
            atv.setTextColor(Color.RED);
        }
}
