package com.osiris.priya.androidviews;

import android.app.Activity;
import android.os.Bundle;

/**
 * Created by priya on 6/15/2016.
 */
public class Test extends Activity{

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.test);
    }
}
