package com.osiris.priya.androidviews;

import android.app.Activity;
import android.graphics.Color;
import android.os.Bundle;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.Spinner;

/**
 * Created by priya on 6/16/2016.
 */
public class testSpinner extends Activity {

    static final String[] months = new String[]{
            "January", "February", "March", "April", "May", "June", "July", "August", "September",
            "October", "November", "December"
    };

    @Override
    public void onCreate(Bundle icicle){
        super.onCreate(icicle);
        setContentView(R.layout.spinner);

        final Spinner spinner = (Spinner) findViewById(R.id.testSpinner);
        ArrayAdapter<String> adapter = new ArrayAdapter<String>(this,R.layout.support_simple_spinner_dropdown_item,months);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinner.setAdapter(adapter);

        final Button firstButton = (Button) findViewById(R.id.enableButton);
        firstButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                changeOption(spinner);
            }
        });

        final Button secondButton = (Button) findViewById(R.id.bgColorButton);
        secondButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                changeTextColor(spinner);
            }
        });
    }
    public void changeOption(Spinner spinner){
        if(spinner.isEnabled()){
            spinner.setEnabled(false);
        }else {
            spinner.setEnabled(true);
        }
    }
    public void changeTextColor(Spinner spinner){

        spinner.setBackgroundColor(Color.RED);
    }
}
